import { getDOMElements } from './dom'

// Hide/show elements in resize
export const windowsResize = () => {
  const button = getDOMElements('#upload_cover_image')

  if (window.innerWidth >= 768 && button.classList.contains('profile__button--hidden')) {
    button.classList.replace('profile__button--hidden', 'profile__button--grayed')
    return
  }

  if (window.innerWidth < 768 && button.classList.contains('profile__button--grayed')) {
    button.classList.replace('profile__button--grayed', 'profile__button--hidden')
    return
  }
}
