// createDOMElement creates a Element or a TextNode
export const createDOMElement = ({ tag, classname, content, ...attrs }) => {
  // We need or a content
  if (!tag && !content) throw 'createDOMElement needs a tag or content'

  // There is no tag, so there is content
  if (!tag) {
    return document.createTextNode(content)
  }

  // Handle when there is a tag and content
  const el = document.createElement(tag)

  // Add classname if applies
  if (classname && typeof classname === 'string') {
    el.className = classname
  }

  // Add attrs
  for (const prop in attrs) {
    el.setAttribute(prop, attrs[prop])
  }

  // Handle content
  if (!content) return el

  let c;

  if (typeof content === 'string') {
    c = document.createTextNode(content)
  }

  if (typeof content === 'object') {
    c = createDOMElement(content)
  }

  el.appendChild(c)
  return el
}

// getDOMElement return the Element from the param passed
// Returns a DOM element, null or a HTMLCollection
export const getDOMElements = str => {
  if (typeof str !== 'string') return

  // Verify if is an ID or and classname
  const i = str.charAt(0)

  if (i === '#') {
    return document.getElementById(str.slice(1))
  }

  if (i === '.') {
    return document.getElementsByClassName(str.slice(1))
  }

  return document.getElementsByTagName(str)
}

// getDOMInnerElements gets the Element(s) from the param passed
// Returns a DOM element, null or a HTMLCollection
// Very basic implementation
export const getDOMInnerElements = str => {
  if (typeof str !== 'string') return

  // Split the str into chunks divided by spaces
  const [ eParent, eModifier, eToSearch ] = str.split(' ')

  const [ parent ] = getDOMElements(eParent)

  // No parent
  if (!parent) return

  // No children
  if (!parent.hasChildNodes()) return parent

  const nodes = []

  for (let i = 0; i < parent.childNodes.length; i++) {
    if (!eToSearch) {
      nodes.push(parent.childNodes[i])
      continue
    }

    if (parent.childNodes[i].tagName === eToSearch.toUpperCase()) {
      nodes.push(parent.childNodes[i])
    }
  }

  return nodes
}

// removeElements remove the Element(s) from the param passed
// Very basic implementation
export const removeElements = str => {
  if (typeof str !== 'string') return

  const elements = str.split(',')

  for (let i = 0; i < elements.length; i++) {
    const nodes = getDOMInnerElements(elements[i].trim())
    nodes.forEach(node => node.remove())
  }
}

// hideElements hide the Element(s) from the param passed
// Very basic implementation
export const hideElements = str => {
  if (typeof str !== 'string') return

  const elements = str.split(',')

  for (let i = 0; i < elements.length; i++) {
    let el = elements[i].trim()
    const nodes = getDOMInnerElements(el)
    nodes.forEach(e => {
      if (!e.tagName) return

      const toHide = ['header', 'h4', 'span']

      if (toHide.includes(e.tagName.toLowerCase())) {
        e.classList.toggle(`${el.replace('.', '')}--hidden`)
      }
    })
  }
}
