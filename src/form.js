import { createDOMElement } from './dom'

export const createForm = () => {
  const form = createDOMElement({
    tag: 'form',
    classname: 'form',
  })

  const header = createDOMElement({
    tag: 'header',
    classname: 'info__header info__header--form',
    content: {
      tag: 'h3',
      content: 'About',
    },
  })

  const buttons = [
    'cancel',
    'save',
  ]

  const buttonsNode = buttons.map(createButton)
  buttonsNode.forEach(node => header.appendChild(node))

  const inputs = [
    'first name',
    'last name',
    'website',
    'phone number',
    'city, state & zip',
  ]

  const inputsNode = inputs.map(createInputGroup)

  form.appendChild(header)
  inputsNode.forEach(node => form.appendChild(node))

  return form
}

const createInputGroup = name => {
  const wrapper = createDOMElement({
    tag: 'div',
    classname: 'form__group',
  })

  const n = name
    .replace(',', '')
    .replace('&', '')
    .split(' ')
    .join('')

  const label = createDOMElement({
    tag: 'label',
    classname: 'form__group--label',
    content: name,
    for: n,
  })

  const input = createDOMElement({
    tag: 'input',
    classname: 'form__group--input',
    name: n,
    id: n,
  })

  wrapper.appendChild(label)
  wrapper.appendChild(input)

  return wrapper
}

const createButton = str => (
  createDOMElement({
    tag: 'button',
    classname: 'form__button',
    content: str,
    name: str,
  })
)
