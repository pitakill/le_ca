import { windowsResize } from './window'
import { createDOMElement, getDOMElements, hideElements, removeElements } from './dom'
import { createForm } from './form'

import styles from './style.scss'

// State of the Dashboard
const state = {
  firstname: 'Jessica',
  lastname: 'Parker',
  website: 'www.seller.com',
  phonenumber: '(949) 325 - 68594',
  citystatezip: 'Newport Bearh, CA',
}

function handleClick(e) {
  e.preventDefault()

  if (this.name === 'save') {
    for (const prop in state) {
      const { value } = getDOMElements(`#${prop}`)
      state[prop] = value
    }

    // Remove form
    removeElements('.form')
    const [ form ] = getDOMElements('.form')
    form.remove()
    // Create info
    hideElements('.info')
    // Write state
    setInfo()
  }

  if (this.name === 'cancel') {
    removeElements('.info')
  }
}

const setInfo = () => {
  const {
    firstname,
    lastname,
    website,
    ...rest
  } = state
  const name = `${firstname} ${lastname}`

  // Set Profile
  let data = { name, ...rest }
  for (const prop in data) {
    const el = getDOMElements(`#profile__${prop}`)
    if (prop === 'name') {
      el.innerHTML = name
      continue
    }
    el.innerText = data[prop]
  }

  // Set Info
  data = { ...data, website }
  for (const prop in data) {
    const el = getDOMElements(`#info__${prop}`)
    if (prop === 'name') {
      el.innerHTML = name
      continue
    }
    el.innerText = data[prop]
  }
}

document.addEventListener('DOMContentLoaded', () => {
  setInfo()
  // Fire the first time when the DOM is loaded
  windowsResize()
  window.addEventListener('resize', windowsResize)

  const edit = getDOMElements('#edit')
  edit.addEventListener('click', e => {
    // Remove elements
    hideElements('.info')

    // Create elements
    const form = createForm()

    let [ info ] = getDOMElements('.info')

    info.appendChild(form)

    const [ headerForm ] = getDOMElements('.info__header--form')

    // Add event listeners
    for (let i = 0; i < headerForm.childNodes.length; i++) {
      const node = headerForm.childNodes[i]
      if (node.tagName.toLowerCase() === 'button') {
        node.addEventListener('click', handleClick)
      }
    }
  })
})
